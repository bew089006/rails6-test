class ApplicationController < ActionController::API
  include ActionController::MimeResponds

  def append_info_to_payload(payload)
    super
    payload[:host] = request.host
  end

  def not_found
    render json: { error: 'not_found' }
  end

  def authorize_request
    header = request.headers['Authorization']
    header = header.split(' ').last if header
    begin
      @decoded = JsonWebToken.decode(header)
      @current_user = User.find(@decoded[:user_id])

      blacklist_jwt = JwtBlacklist.where(user_id: @decoded[:user_id]).last 

      unless DateTime.parse(Time.at(@decoded["exp"]).to_s) > DateTime.parse(blacklist_jwt.exp)
        render json: { errors: "unauthorized" }, status: :unauthorized
      end

    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized
    rescue JWT::DecodeError => e
      render json: { errors: e.message }, status: :unauthorized
    end
  end
end
