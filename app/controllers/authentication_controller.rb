class AuthenticationController < ApplicationController
  include ::Devise::Controllers::SignInOut

  before_action :authorize_request, except: :sign_in
  
  def sign_in
    @user = User.find_by_email(params[:email])
    if @user&.authenticate(params[:password])
      token = JsonWebToken.encode(user_id: @user.id)
      time = Time.now + 24.hours.to_i
      render json: { 
        token: token, 
        exp: time.strftime("%m-%d-%Y %H:%M"),
        username: @user.username
      }, status: :ok

    else
      render json: { error: 'unauthorized' }, status: :unauthorized
    end
  end

  def sign_out
    token = request.headers.env['HTTP_AUTHORIZATION'].to_s.split('Bearer ').last
    revoke_token(token)

    request.delete_header('Authorization')

    render json: {
      status: "logged out"
    }
  end

  private

  def login_params
    params.permit(:email, :password)
  end

  def revoke_token(token)
    # Decode JWT to get jti and exp values.
    jti = JsonWebToken.decode(token)


    # Add record to blacklist.
    sql_blacklist_jwt = "INSERT INTO jwt_blacklists (user_id, exp, created_at, updated_at) VALUES ('#{ jti[:user_id] }', '#{ Time.at(jti[:exp]) }', now(), now());"
    ActiveRecord::Base.connection.execute(sql_blacklist_jwt)
  end
end
