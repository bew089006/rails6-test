class TodosController < ApplicationController
  before_action :authorize_request

  def index
    todos = Todo.all

    render json: {todos: todos}
  end

  def create
    todo = Todo.create(permitted_params)

    render json: {todo: todo}
  end

  def show
    todo = Todo.find(params[:id])

    render json: {todo: todo}
  end

  def update
    todo = Todo.find(params[:id])
    todo.update(permitted_params)

    render json: {todo: todo}
  end

  def destroy
    todo = Todo.find(params[:id])

    if  todo.destroy
      status = "success"
    else
      status = "failed"
    end
    

    render json: {status: status}
  end

  private
  def permitted_params
    params.permit(:title, :description)
  end
end
