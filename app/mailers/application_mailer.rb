class ApplicationMailer < ActionMailer::Base
  default from: "bew089006@gmail.com"
  layout 'mailer'
end
