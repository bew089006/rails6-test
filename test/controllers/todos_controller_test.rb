require 'test_helper'

class TodosControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    todo = Todo.create(title: "test01", description: "0001")

    get todos_url

    pp response
  end
  focus
  test "should create todo" do
    post todos_url, params: { title: "bew089001", description: "test01" }

    # pp response.body
  end
  
  test "should show todo" do
    todo = Todo.create(title: "test02", description: "0002")

    get "/todos/#{todo.id}"

    pp response.body
  end

  test "sholud update todo" do
    todo = Todo.create(title: "test03", description: "0003")

    put "/todos/#{todo.id}", params: {
      title: "update03",
      description: "update03"
    }

    pp response.body
  end

  test "sholud delete todo" do
    todo = Todo.create(title: "test04", description: "0004")

    delete "/todos/#{todo.id}"

    pp response.body
  end
end
