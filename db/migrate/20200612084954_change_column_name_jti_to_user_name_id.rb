class ChangeColumnNameJtiToUserNameId < ActiveRecord::Migration[6.0]
  def change
    rename_column :jwt_blacklists, :jti, :user_id
  end
end
