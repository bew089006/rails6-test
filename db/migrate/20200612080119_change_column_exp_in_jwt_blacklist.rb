class ChangeColumnExpInJwtBlacklist < ActiveRecord::Migration[6.0]
  def change
    change_column :jwt_blacklists, :exp, :string
  end
end
