class CreateTodos < ActiveRecord::Migration[6.0]
  def change
    create_table :todos do |t|
      t.string :title
      t.string :description
      t.datetime :start_date
      t.datetime :end_date
      t.timestamps
    end
  end
end
