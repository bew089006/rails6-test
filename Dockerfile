FROM tqld/insr-base:ruby-2.7-alpine
 
ENV LANG en_US.UTF-8 
ENV LANGUAGE en_US:en 
ENV LC_ALL en_US.UTF-8
 
WORKDIR /app
 
RUN apk add --no-cache --update \
 build-base \
 bash \
 tzdata \
 mysql-dev