Rails.application.routes.draw do
  resources :users, param: :_username
  post '/auth/sign_in', to: 'authentication#sign_in'

  delete '/auth/sign_out', to: 'authentication#sign_out'
  # get '/*a', to: 'application#not_found'
  # get 'todo/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :todos
end
